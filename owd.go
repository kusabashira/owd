package main

import (
	"fmt"
	"os"
	"os/exec"
)

func main() {
	err := exec.Command("explorer", ".").Start()
	if err != nil {
		fmt.Fprintln(os.Stderr, "owd:", err)
		os.Exit(1)
	}
	os.Exit(0)
}
