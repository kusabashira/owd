owd
====================
owd is a command which open working directory with explorer.

Platform
====================
Microsoft Windows

Usage
====================
hit the following in your terminal.

	$ owd

It will open working directory with explorer.

License
====================
MIT LICENCE

Author
====================
Kusabashira <kusabashira227@gmail.com>
